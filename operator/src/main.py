import kopf


@kopf.on.create('questioners.speedcoding.marc-cesarine.fr')
def create_fn(spec, **kwargs):
    print(f"And here we are! Creating: {spec}")
    return {'message': 'hello world'}  # will be the new status
