#! /usr/bin/env python3

import os
import random
import threading
import time
import urllib.error
import urllib.parse
import urllib.request
from prometheus_client import Counter, start_http_server


class questionThread(threading.Thread):
    def __init__(
        self,
        question,
        answer,
        question_type,
        host,
        port,
        success_metrics,
        failure_http_metrics,
        failure_metrics,
    ):
        threading.Thread.__init__(self)
        self.question = question
        self.answer = answer
        self.question_type = question_type
        self.target_host = host
        self.target_port = port
        self.success_metrics = success_metrics
        self.failure_http_metrics = failure_http_metrics
        self.failure_metrics = failure_metrics

    def run(self):
        print(f"Question : {self.question}", flush=True)
        encoded_question = urllib.parse.urlencode({"question": self.question})
        request = urllib.request.Request(
            f"http://{self.target_host}:{self.target_port}/?{encoded_question}"
        )
        try:
            with urllib.request.urlopen(request) as response:
                data = response.read().decode("utf-8")
                if data == self.answer:
                    print(f"Correct response : {data}")
                    self.success_metrics.labels(question_type=self.question_type).inc(1)
                else:
                    print(f"Bad response : {data}")
                    self.failure_http_metrics.labels(
                        question_type=self.question_type
                    ).inc(1)
        except urllib.error.HTTPError as error:
            print(f"HTTP error : {error.code}")
            self.failure_http_metrics.labels(question_type=self.question_type).inc(1)
        except:
            print(f"Connexion error")
            self.failure_metrics.labels(question_type=self.question_type).inc(1)


class Questionner:
    def __init__(self):
        self.db_host = os.getenv("DB_HOST")
        print(self.db_host)
        self.target_host = os.getenv("TARGET_HOST")
        self.target_port = os.getenv("TARGET_PORT")
        print(f"target = {self.target_host}:{self.target_port}")
        self.success_metrics = Counter(
            "success_responses", "Success responses", ["question_type"]
        )
        self.failure_http_metrics = Counter(
            "failed_http_responses", "Failed HTTP responses", ["question_type"]
        )
        self.failure_metrics = Counter(
            "failed_responses", "Failed responses", ["question_type"]
        )

    def run(self):
        while True:
            self.theme = "addition"
            questions = self.generate_questions(self.theme)
            self.ask(questions, self.theme)

    def generate_questions(self, theme):
        result = []
        for i in range(5):
            if theme == "addition":
                rand1 = random.randint(0, 1000)
                rand2 = random.randint(0, 1000)
                result.append((f"{rand1} + {rand2}", str(rand1 + rand2)))
        return result

    def ask(self, questions, theme):
        threads = []
        for question, answer in questions:
            questionThr = questionThread(
                question,
                answer,
                theme,
                self.target_host,
                self.target_port,
                self.success_metrics,
                self.failure_http_metrics,
                self.failure_metrics,
            )
            questionThr.start()
            threads.append(questionThr)
            time.sleep(5)

        for thread in threads:
            thread.join()


if __name__ == "__main__":
    start_http_server(8000)
    questionner = Questionner()
    questionner.run()
